## Hosting the demo code in your s3 bucket

You will need to create an s3 bucket to host the static web page.
In order to sync this code to s3 you need an aws account and you need to have the aws cli installed.

[AWS CLI Installation](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)


To sync the code to your s3 bucket, run the following convenience script using bash to sync the code to your s3 bucket
```bash
BUCKET_NAME="my-bucket" scripts/sync
```

Once this is done, make sure that you have enabled public read access to your bucket
and then [enable static website hosting](https://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteHosting.html).

## Running the demo

Go to the page `/accountAccess.html` in your static website, or check it out at
https://tc-summit-oauth-demo.s3-website.us-east-2.amazonaws.com/currentTrack.html

- Make sure your spotify account is playing a song before loading the page.
- The demo uses cookies to store the access token. If the access token is expired you will need to manually remove the cookie and reload the page.

The redirection uri and client id used are hardcoded into the code.  You will need to change these to use your own client id and redirectio uri.


